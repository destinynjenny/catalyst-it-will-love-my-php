<?php

run();

/**function: the main function of this script*/
function run() {
    try {
        $shortopts = "u:p:h:";      //parse short commands
        $longopts = array(          //parse long commands
            "file:",
            "help",
            "create_table",
            "dry_run",
        );

        $commands = getopt($shortopts, $longopts);  //fetch the command-line parameters
        $db_name = 'catalyst';
        $option = validateCommands($commands);

        switch($option)
        {
            case "help":
                showHelp();
                break;
            case "create_table":
                createTable($commands['h'], $commands['u'], $commands['p'], $db_name);
                break;
            case "dry_run":
                print("launch dry run test...\n\n");
                print("test read CSV file...\n\n");
                readCSV($commands['file']);
                print("successfully read CSV file\n\n");
                print("test DB connection...\n\n");
                print("create DB table 'users'...\n\n");
                createTable($commands['h'], $commands['u'], $commands['p'], $db_name);
                print("dry run finished... all good\n\n");
                break;
            case "file":
                $data = readCSV($commands['file']);
                createTable($commands['h'], $commands['u'], $commands['p'], $db_name);
                insertDB($commands['h'], $commands['u'], $commands['p'], $db_name, $data);
                break;
            default:
                echo "Unacceptable options \n";
        }
        exit("Script has been successfully executed! \n");
    } catch (Exception $e) {
        die($e->getMessage());
    }
}


/**function: validate command*/
function validateCommands($commands) {
    array_key_exists("file",$commands) ? $file_path = $commands['file'] : $file_path = "";
    array_key_exists("h",$commands) ? $server_name = $commands['h'] : $server_name = "";
    array_key_exists("u",$commands) ? $user_name = $commands['u'] : $user_name = "";
    array_key_exists("p",$commands) ? $password = $commands['p'] : $password = "";
    $option = "";

    if (array_key_exists("help", $commands)) {
        $option = "help";
    } 
    else if (array_key_exists("create_table", $commands)) {
        if (!$server_name || !$user_name || !$password) {
            throw new Exception("Please input servername, username and password. \n"); 
        } else {
            $option = "create_table";
        }
    }
    else if (array_key_exists("dry_run", $commands)) {
        if (!$server_name || !$user_name || !$password || !$file_path) {
            throw new Exception("Please input file path, servername, username and password. \n"); 
        } else {
            $option = "dry_run";
        }
    }
    else if (array_key_exists("file", $commands)) {
        if (!$server_name || !$user_name || !$password || !$file_path) {
            throw new Exception("Please input file path, servername, username and password. \n"); 
        } else {
            $option = "file";
        }
    }
    else {
        throw new Exception("Invalid Command! Please try again or type --help to get further information. \n");
    }
    return $option;
}


/**function: show the command line directive.*/
function showHelp() {
    print("\n");
    print("Help Guideline\n");
    print("csv import db script--Jiahui Peng--version 1\n\n");
    print("--file [csv file name] – this is the name of the CSV to be parsed. \n");
    print("--create_table – this will cause the MySQL users table to be built (and no further
    action will be taken). \n");
    print("--dry_run – this will be used with the --file directive in case we want to run the script but not insert into the DB. All other functions will be executed, but the database won't be altered\n");
    print("-u – MySQL username \n");
    print("-p – MySQL password\n");
    print("-h – MySQL host\n");
    print("--help – which will output the above list of directives with details.\n\n");
}


/**function: fetch csv file detail, then return all rows as an array.*/
function readCSV($file_path) {
    try {
        if (file_exists($file_path)) {
            $file = fopen($file_path,'r');
            $csv_data = [];
            $row_num = 0;   // skip the description rows, which is index 0
            while ($row = fgetcsv($file)) {
                if ($row_num > 0) {
                    $row = formatData($row);
                    if ($row) {
                        array_push($csv_data,$row);
                    }
                }
                $row_num += 1;
            }
            return $csv_data;
        } else {
            throw new Exception("No such file, please check the path \n");
        }
    } catch (Exception $e){
        die("Error Message: ".$e->getMessage());
    }
}


/**function: 1.regular express filter the name and surname;
         2.validate the email format;  */
function formatData($line) {
    try {
        $format_line = [];
        for($i = 0; $i < 3; $i++) {
            if ($i == 0 || $i == 1) {
                $pattern = '/[a-zA-Z]+\'{0,1}[a-zA-Z]+/';
                preg_match($pattern, $line[$i], $matches);
                if ($matches) {
                    $matches = ucfirst(strtolower($matches[0]));
                    array_push($format_line, $matches);
                } else {
                    throw new Exception("name or surname: ".$line[$i]." is illegal format \n");
                }
            } else {
                $line[2] = preg_replace('/[[:space:]]+/', '', $line[2]);    // remove the tail and head space or tab
                if (filter_var($line[2], FILTER_VALIDATE_EMAIL)) {
                    array_push($format_line, $line[2]);
                } else {
                    throw new Exception("Email: ".$line[2]." is illegal format.\nThis row won't be inserted into DB.\n\n");
                }
            }
        }
        return $format_line;
    } catch (Exception $e) {
        print_r("Error Message: ".$e->getMessage());
        return false;
    }
}


/**function: check the DB connect, before SQL operations with DB.*/
function connectDB($servername, $username, $password) {
    try {
        $conn = new mysqli($servername, $username, $password);
        if ($conn->connect_error) {
            throw new Exception("Error Message: ".$conn->connect_error.".\n\nPlease check servername, username and password.\n");
        } 
        return $conn;
    } catch (Exception $e) {
        mysqli_close($conn);
        die($e->getMessage());
    }
}


/**function: create a 'users' table*/
function createTable($servername, $username, $password, $dbname) {
    try {
            $conn = connectDB($servername, $username, $password);  //check the database connection
            if (! $conn->select_db($dbname)) {      //check if the database is exists
                throw new Exception("Error Message: the default database name is wrong, please check database name or create the database by root. \n");
            }
            $sql = "CREATE TABLE IF NOT EXISTS users(
                ID INT NOT NULL AUTO_INCREMENT,
                name VARCHAR(255),
                surname VARCHAR(255),
                email VARCHAR(320) NOT NULL UNIQUE,
                PRIMARY KEY (ID)
            );";
            if ( !$conn->query($sql)) {
                throw new Exception("Error Message: SQL fails to create table. \n");
            }
            mysqli_close($conn);
            print_r("Table USERS is successfully created/rebuilt.\n\n");
            return true;
        
    } catch (Exception $e){
        mysqli_close($conn);
        die($e->getMessage());
    }
}


/**function: sql insert DB function*/
function insertDB($servername, $username, $password, $dbname, $data) {
    try{
        $conn = connectDB($servername, $username, $password);   //check the database connection
        // createTable($servername, $username, $password, $dbname);     //check the database and table, if table does not exists, create it.
        // $conn = new mysqli($servername, $username, $password);
        foreach($data as $row) {
            $name = $conn->real_escape_string($row[0]);
            $surname = $conn->real_escape_string($row[1]);
            $email = $conn->real_escape_string($row[2]);
            //duplcate email check.
            $sql = "SELECT * FROM catalyst.users WHERE email = '{$email}';";
            $result = $conn->query($sql);
            $result = mysqli_fetch_array($result);
            if ($result) {
                print_r("Warning : There is a duplicate email: ".$result['email']."\n");
                Print_r("This record won't be inserted \n name: ".$result['name']."\n surname: ".$result['surname']."\n email: ".$result['email']."\n\n");
                continue;
            }
            $sql = "INSERT INTO catalyst.users (name, surname, email) VALUES ('{$name}','{$surname}','{$email}');";
            if (! $conn->query($sql) ){
                throw new Exception("Error Message: SQL fails to insert row. \n");
            }
        }
        mysqli_close($conn);
        print_r("CVS data has been successfully inserted into database: '".$dbname."' table: 'users' \n");
        return true;
    } catch (Exception $e){
        mysqli_close($conn);
        die($e->getMessage());
    }
}

?>