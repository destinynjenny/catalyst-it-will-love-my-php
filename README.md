# Catalyst IT will love my PHP

This is an interesting PHP code test from Catalyst IT Australia.
This project is about PHP script with Mysql.
Let's rock it :)

1. [Project Design](#design)
2. [How To Lunach The Project With Docker](#docker)

# 1. Project Design<a name='design'></a>

## 1.1 Run The Project
There are 2 ways to run this porject:

1. Copy my files(user_upload.php and foobar) in the **scr**, and paste them to your computer, then run it. To get more information about my code and function, please check my [function.](#function)
2. Runing it with [my docker containers.](#docker)

## 1.2 Project Folder Structure
- mysql
    - **Dockerfile**: The Mysql server docker container with specific configuration of DB.
- server
    - **Dockerfile**: The nginx server docker container.
    - **nginx-fpm.conf**: configuration for nginx server.
    - **supervisord.conf**: create a deamon for this container to make the container keep running.
- scr
    - **foobar.php**: script for the logical test 
    - **user_upload.php**: script for the script test
    - **users.csv**: csv file for test one

## 1.3 Function<a name='function'></a>

| Function Name | Parameters | Description | Return Value |
| :---      |  :------  | :--- | :--- |
| **run** | | the main function of this script. | Try to execute the script, and it will throw the error when it occur. |
| **validateCommands** | **$commands**: parameters parsed from command line. | It will Identify which instruction will be used to execute my script, and it will also validate the command| Return one of **help/create_table/dry_run/file**.<br>Otherwise, throw error and exit script. |
| **showHelp**   |  | It will show the command line directives in the STDOUT (terminal). | There is no return value. |
| **readCSV** | **$file_path**: the csv file path. | It will fetch csv file detail, then return all rows as an array. | Return **array** conatins all legal format data if it is successful. <br>Otherwise, throw error and exit script.|
| **formatData** | **$line**: each row of the csv file. | This function will read line data, and filter name and surname by **regular express**, and validate the email colunm as well. <br>The illegal format email won't be stored in array, and an error message will be outputed to STDOUT. | Return **array** contains the legal format line data. <br>Otherwise, return **false**. |
| **connectDB** | **$servername**: database server name.<br> **$username**: databse user name.<br> **$password**: database password. | It will test the database connection before doing any Mysql operation.| Return **DB object** if it is successful.<br>Otherwise, throw a db connection error. |
| **createTable** | **$servername**: database server name.<br> **$username**: databse user name.<br> **$password**: database password.<br> **$dbname**: should be the default database **catalyst**. | It will create the 'users' table. | Return **true** if it is successful.<br>Otherwise, throw a db connection error. |
| **insertDB** | **$servername**: database server name.<br> **$username**: databse user name.<br> **$password**: database password.<br> **$dbname**: should be the default database **catalyst**.<br> **$data**: they array data from function **readCSV**. | It will execute mysql insert operation. | Return **true** if it is successful.<br>Otherwise, throw a db connection error. |


## 1.4 Script Command
- --file [csv file name] – this is the name of the CSV to be parsed
- --create_table – this will cause the MySQL users table to be built (and no further
- action will be taken)
- --dry_run – this will be used with the --file directive in case we want to run the script but not insert into the DB. All other functions will be executed, but the database won't be altered
- -u – MySQL username
- -p – MySQL password
- -h – MySQL host
- --help – which will output the above list of directives with details.


**For Example**:

If you type command line:
```shell
php user_upload.php --file <file_name> -u <username> -p <password> -h <host>
``` 
My script will run the whole process, which includes read file, create table, and try to insert data into table. 

## 1.5 About Database
I assume the database name and database table name is defult as:

- **DB name**: catalyst
- **DB table**: users

Table Structure:
```sql
CREATE TABLE users(
    ID INT NOT NULL,
    name VARCHAR(255),
    surname VARCHAR(255),
    email VARCHAR(320) NOT NULL UNIQUE,
    PRIMARY KEY (ID)
);
```

**Note**
If you run my project with my docker env, you could access **[127.0.0.1:8130](http://127.0.0.1:8130)** to the **phpmyadmin** page, and login with the username and password provided
by my [docker database configuration.](#dbconfig)


# 2. How To Launch The Project With Docker<a name='docker'></a>

## 2.1 My Docker Env 
The script test requires a PHP(7) and Mysql(5.7) env. In order to create such a testing or running env, I create a docker env first. 
If you want to run my test script in docker env, You could directly launch my docker env by input **docker-compose build**, then **docker-compose up**. I have written a docker script for this project env, which includes mysql and Ubuntu with PHP, and you can access mysql from **phpmyadmin** in your browser by **'127.0.0.1:8130'**.

The database host please check [here](#host).
The database user, password please check [here](#dbconfig).

## 2.2 My Container Detail<a name='container-name'></a>
When the docker containers are running, you will have three running containers:

| Docker | Description | Docker Name |
| :--- | :--- | :--- |
| Ubuntu | It is Ubuntu env with PHP7, **and we run the script in this container**. | **catalyst-it-will-love-my-php_server_1** |
| Mysql | It is Mysql env with 5.7 version. | **catalyst-it-will-love-my-php_mysql_1** |
| PHPMyadmin | It is built for checking if the data is imported from the script successfully. <br>Access it: [127.0.0.1:8130](http://127.0.0.1:8130) | **catalyst-it-will-love-my-php_phpmyadmin_1** |

To get info about these docker containers, please input:

```shell
docker ps --no-trunc
```
Or please check the [docker command line](https://docs.docker.com/engine/reference/commandline/ps/)


## 2.3 Go Inside My Docker Container

To go inside **ubuntu container** to run the script, we need get ubuntu container ID at first.
Just use our [ubuntu container name](#container-name): **catalyst-it-will-love-my-php_server_1** and input:

```shell
docker ps -aqf "name=catalyst-it-will-love-my-php_server_1"
```

When we get the ID:

```shell
docker exec -it <ID> /bin/bash
```

Now, You have already go inside the Ubuntu container's terminal, and go to the path **/var/project**, to find my php script, and then you can run it!

For example:
```shell
destiny:catalyst-it-will-love-my-php destiny$ docker ps -aqf "name=catalyst-it-will-love-my-php_server_1"
78967a2a288a
XXX:catalyst-it-will-love-my-php destiny$ docker exec -it 78967a2a288a /bin/bash
root@78967a2a288a:/# ls
bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@78967a2a288a:/# cd /var/project/
root@78967a2a288a:/var/project# ls
foobar.php  user_upload.php  users.csv
root@78967a2a288a:/var/project# php user_upload.php --file ./users.csv -h 172.29.0.2 -u cuser -p cpassword
```

## 2.2 About Launch Docker
To run the docker env, please follow the command lines:

**Build the docker(This command line only be used at the first time, just wait a few mins)**
```shell
docker-compose build
```
**Launch the docker container by typing this command:**
```shell
docker-compose up
```

**Stop the docker containers**
```shell
docker-compose stop
```

**Remove the docker containers**
```shell
docker-compose down
```

## 2.3 My Docker Database Container Configration:<a name='dbconfig'></a>

**Database Configure**:

| Name | Description |
| ---      |  ------  |
| Database Name   | catalyst |
| User Name | cuser |
| User Passrod   | cpassword | 
| Root Password | rootpassword |

**Note** About the **database server host**, please check [2.4](#host).

## 2.4 Database Server Host:<a name='host'></a>

The docker container will randomly assign the ip for MYSQL container, so get the server hostname by:

```shell
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' catalyst-it-will-love-my-php_mysql_1
```

**catalyst-it-will-love-my-php_mysql_1** is the Mysql [container name](#container-name).

